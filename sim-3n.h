class sim_3n {

    public:

// members -- note, these need to be defined in the same order in which they appear in the branch declaration

    typedef struct {
       	double t, x, y, z, x_mona, y_mona, z_mona, vx_cm, vy_cm, vz_cm, v_cm, vx_lab, vy_lab, vz_lab, v_lab, tx, ty; // t, x, y, z, q for individual neutron hit events   
       	double edecay, vdecay; 
    } Create_neutron;

    typedef struct {
       	double vx_cm, vy_cm, vz_cm, v_cm, vx_lab, vy_lab, vz_lab, v_lab, tx, ty; // velocity of fragment in cm and lab frames   
    } Create_fragment;

    typedef struct {
    	double t[3], x[3], y[3], z[3], vx[3], vy[3], vz[3], v[3]; // t, x, y, z, vx, vy, vz for hits 1 - 3 
    } Create_hit;
};
