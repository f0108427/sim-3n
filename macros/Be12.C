{
//========= Macro generated from object: be12/Graph
//========= by ROOT version6.14/00

    t->SetAlias("xtx1","focus.tx-0.911*focus.x-0.00585*focus.x*focus.x-0.0000627*focus.x*focus.x*focus.x");
   TCutG *be12 = new TCutG("be12",20);
   be12->SetVarX("tof.pot_thin");
   be12->SetVarY("xtx1");
   be12->SetTitle("Graph");
   be12->SetFillStyle(1000);
   be12->SetPoint(0,48.3526,-72.2177);
   be12->SetPoint(1,46.4716,-52.2324);
   be12->SetPoint(2,44.4219,-25.5853);
   be12->SetPoint(3,43.3729,-17.9718);
   be12->SetPoint(4,42.6374,-14.8788);
   be12->SetPoint(5,41.9863,-18.2097);
   be12->SetPoint(6,41.4316,-23.444);
   be12->SetPoint(7,41.504,-30.1058);
   be12->SetPoint(8,42.0345,-50.5669);
   be12->SetPoint(9,43.192,-65.5559);
   be12->SetPoint(10,43.7466,-71.266);
   be12->SetPoint(11,44.8921,-83.4);
   be12->SetPoint(12,46.2305,-98.8648);
   be12->SetPoint(13,47.3036,-114.33);
   be12->SetPoint(14,48.9796,-128.129);
   be12->SetPoint(15,49.4378,-130.984);
   be12->SetPoint(16,50.74,-132.412);
   be12->SetPoint(17,50.6435,-98.389);
   be12->SetPoint(18,48.3526,-71.9798);
   be12->SetPoint(19,48.3526,-72.2177);
   //be12->Draw("");
    
    
    TCutG *good_padsum = new TCutG("good_padsum",12);
    good_padsum->SetVarX("crdc2.padsum");
    good_padsum->SetVarY("crdc1.padsum");
    good_padsum->SetTitle("Graph");
    good_padsum->SetFillStyle(1000);
    good_padsum->SetPoint(0,32349.3,35191.7);
    good_padsum->SetPoint(1,24117.9,37157.1);
    good_padsum->SetPoint(2,16191.4,34554.3);
    good_padsum->SetPoint(3,12499.1,26427.3);
    good_padsum->SetPoint(4,9992.46,18884.7);
    good_padsum->SetPoint(5,8840.74,13785.4);
    good_padsum->SetPoint(6,10399,8739.23);
    good_padsum->SetPoint(7,19544.9,11395.1);
    good_padsum->SetPoint(8,31739.6,16706.8);
    good_padsum->SetPoint(9,36482,24090.2);
    good_padsum->SetPoint(10,32484.8,35085.5);
    good_padsum->SetPoint(11,32349.3,35191.7);
    //good_padsum->Draw("");
    
    TCutG *cutg = new TCutG("cut_large",6);
    cut_large->SetVarX("nsi12");
    cut_large->SetVarY("a012");
    cut_large->SetTitle("Graph");
    cut_large->SetFillStyle(1000);
    cut_large->SetLineColor(2);
    cut_large->SetLineWidth(4);
    cut_large->SetPoint(0,-6006.1,0.672093);
    cut_large->SetPoint(1,-6013.73,180.845);
    cut_large->SetPoint(2,-300.0,180.541);
    cut_large->SetPoint(3,-300.0,-0.84451);
    //cut_large->SetPoint(2,-127.564,180.541);
    //cut_large->SetPoint(3,-112.295,-0.84451);
    cut_large->SetPoint(4,-6006.1,0.065452);
    cut_large->SetPoint(5,-6006.1,0.672093);
    //cut_large->Draw("");
    
    TCutG *cut_small = new TCutG("cut_small",7);
    cut_small->SetVarX("nsi12");
    cut_small->SetVarY("a012");
    cut_small->SetTitle("Graph");
    cut_small->SetFillStyle(1000);
    cut_small->SetLineColor(2);
    cut_small->SetLineWidth(4);
    cut_small->SetPoint(0,201.572,180.238);
    cut_small->SetPoint(1,187.671,85.0);
    cut_small->SetPoint(2,5956.81,85.0);
    cut_small->SetPoint(3,5929.01,180.845);
    cut_small->SetPoint(4,187.671,180.845);
    cut_small->SetPoint(5,201.572,178.721);
    cut_small->SetPoint(6,201.572,180.238);
    //cut_small->Draw("");
    
}
     
