// *****
// sim-3n.cpp 
// Simulation for 3 neutrons using ROOT random function generators parameterized by BreitWigner and Uniform
//
// W.F. Rogers 4/2022
//
// For future development:
// 	Single neutron scattering according to LANSCE results
// 	Edges to MoNA to allow neutrons to pass through
// 	Include fragment recoil
// 	Include target straggling and energy loss?
//
// Future uses to include:
// 	Assess effect of causality filters on decay energy results
// 	Create ANSI plots for 1 and 2 neutron signatures (cleaner than current STMONA simulations)
//
//
// *****

#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <math.h>
#include "sim-3n.h"

// root header files
#include </home/rogers/root/include/TFile.h>
#include </home/rogers/root/include/TTree.h>
#include </home/rogers/root/include/TTreeIndex.h>
#include </home/rogers/root/include/TBranchElement.h>
#include </home/rogers/root/include/TH1.h>
#include "/home/rogers/root/include/TRandom.h"
#include "/home/rogers/root/include/TCanvas.h"

using namespace std;

int main(int argc, char** argv)
{
//  if (argc < 2) {
  // Tell the user how to run the program
//    std::cerr << "Usage: " << argv[0] << " Configuration filename " >> endl;
//    return 0;
//  }  

//  Double_t vx_cm, vy_cm, vz_cm, vx_lab, vy_lab, vz_lab, v_lab; 
  Double_t gamma_lab, v_lab;
  Double_t c_ = 29.979; // cm/ns
  Double_t mn_c2 = 939.565; // MeV
  Double_t ebeam, darray, edecay_peak_1, edecay_width_1, edecay_peak_2, edecay_width_2, edecay_peak_3, edecay_width_3;
  Double_t first_peak,_3body_peak, _3body_width;
  Double_t att_length, mona_res;
  Int_t nevents;
  string inputline;

  ifstream config_file;
  string config_file_name[2];
  if(argc == 3){ //check for optional config file
    config_file_name[1] = argv[2];
  } else {
    config_file_name[1] = "sim-3n.cfg"; //default config file name
    config_file_name[0] = "~/";
  }

  config_file.open(config_file_name[1],ios::in);
  string inputline1;
  string inputline2;
  string inputline3;
  string inputline4;
  string inputline5;

  if (config_file.is_open()) {
    getline(config_file,inputline1);
    istringstream in1(inputline1);
    in1 >> ebeam >> darray >> mona_res >> att_length >> nevents;

    getline(config_file,inputline2); 
    istringstream in2(inputline2);
    in2 >> edecay_peak_1 >> edecay_width_1;
    
    getline(config_file,inputline3); 
    istringstream in3(inputline3);
    in3 >> edecay_peak_2 >> edecay_width_2;
    
    getline(config_file,inputline4); 
    istringstream in4(inputline4);
    in4 >> edecay_peak_3 >> edecay_width_3;

    getline(config_file,inputline5); 
    istringstream in5(inputline5);
    in5 >> _3body_peak >> _3body_width;

  } else {
    cout << "Config file did not open properly" << endl;
    return 0;
  }

  v_lab = c_*sqrt(1-(pow(mn_c2/(ebeam+mn_c2),2)));
  gamma_lab = sqrt(1/(1-pow(v_lab/c_,2)));

  cout << " ebeam: " << ebeam << " v_lab: " << v_lab << " gamma_lab: " << gamma_lab << " darray: " << darray << " mona_res: " << mona_res << " att_length: " << att_length << " nevents: " << nevents << endl;
  cout << " edecay_peak_1: " << edecay_peak_1 << " edecay_width_1: " << edecay_width_1 << endl;
  cout << " edecay_peak_2: " << edecay_peak_2 << " edecay_width_2: " << edecay_width_2 << endl;
  cout << " edecay_peak_3: " << edecay_peak_3 << " edecay_width_3: " << edecay_width_3 << endl;
  cout << " _3body_peak: " << _3body_peak << " _3body_width: " << _3body_width << endl;

  TRandom r;
  sim_3n::Create_neutron n1; 
  sim_3n::Create_neutron n2; 
  sim_3n::Create_neutron n3;
  sim_3n::Create_hit hit;
  
//  if(argc == 3){ 
//    char * rootfile_name = argv[1];
//  } else {
//    char * rootfile_name = "sim-3n.root"; //default root file name
//  }
  char* outfile = argv[1];  
  TFile* ofile = new TFile(outfile,"recreate");

  TTree *t = new TTree("t","3n-sim tree");
  t->Branch("n1",&n1,"t/D:x/D:y/D:z/D:x_mona/D:y_mona/D:z_mona/D:vx_cm/D:vy_cm/D:vz_cm/D:vx_lab:vy_lab/D:vz_lab/D:edecay/D:vdecay/D");
  t->Branch("n2",&n2,"t/D:x/D:y/D:z/D:x_mona/D:y_mona/D:z_mona/D:vx_cm/D:vy_cm/D:vz_cm/D:vx_lab:vy_lab/D:vz_lab/D:edecay/D:vdecay/D");
  t->Branch("n3",&n3,"t/D:x/D:y/D:z/D:x_mona/D:y_mona/D:z_mona/D:vx_cm/D:vy_cm/D:vz_cm/D:vx_lab:vy_lab/D:vz_lab/D:edecay/D:vdecay/D");
  t->Branch("hit",&hit,"t_hit[5]/D:x_hit[5]/D:y_hit[5]/D:z_hit[5]/D:vx_hit[5]/D:vy_hit[5]/D:vz_hit[5]/D:edecay_hit[5]/D:vdecay_hit[5]/D");

  for (Int_t num_evt=0;num_evt<nevents;num_evt++) {
	// Determine neutron kinetic energies of decay
//	n1.edecay = r.BreitWigner(edecay_peak_1, edecay_width_1); // edecay value based on random sampling over Breit-Wigner lineishape 
//	n1.vdecay = c_*sqrt(1-pow(mn_c2/(n1.edecay+mn_c2),2)); // velocity of decaying neutron
//     	r.Sphere(n1.vx_cm, n1.vy_cm, n1.vz_cm, n1.vdecay); // randomize directions of velocity vectors with tips over surface of sphere

	// First of two phase space neutrons
//	first_peak = r.Gaus(r.Uniform(0.12,0.16),0.04);
	first_peak = r.Gaus(0.14,0.06); // first of two Be14 neutrons assigned peak with average slightly above 140 keV since higher energy neutrons tend to arrive earlier
	if (first_peak<0) {first_peak = 0.0;}
	n2.edecay = r.BreitWigner(first_peak, edecay_width_2);
	n2.vdecay = c_*sqrt(1-pow(mn_c2/(n2.edecay+mn_c2),2)); // vdecay value
       	r.Sphere(n2.vx_cm, n2.vy_cm, n2.vz_cm, n2.vdecay);
	// Second phase space neutron
	n3.edecay = (r.BreitWigner(_3body_peak, _3body_width)) - n2.edecay;
	n3.vdecay = c_*sqrt(1-pow(mn_c2/(n3.edecay+mn_c2),2)); // vdecay value
       	r.Sphere(n3.vx_cm, n3.vy_cm, n3.vz_cm, n3.vdecay);

	// Transform quantities to the laboratory frame
/*	n1.vx_lab = (n1.vx_cm)/(gamma_lab*(1+n1.vx_cm*v_lab/pow(c_,2)));
	n1.vy_lab = (n1.vy_cm)/(gamma_lab*(1+n1.vy_cm*v_lab/pow(c_,2)));
	n1.vz_lab = (n1.vz_cm+v_lab)/(1+n1.vz_cm*v_lab/pow(c_,2));
	n1.z = darray + r.Exp(att_length);	
	n1.t = (n1.z)/(n1.vz_lab);
	n1.x = (n1.vx_lab)*(n1.t);
        n1.y = (n1.vy_lab)*(n1.t);
	n1.x_mona = n1.x + r.Gaus(0,3);
	n1.y_mona = round(n1.y/10)*10;
	n1.z_mona = int(n1.z/10)*10 + 5;
*/
	n2.vx_lab = (n2.vx_cm)/(gamma_lab*(1+n2.vx_cm*v_lab/pow(c_,2)));
	n2.vy_lab = (n2.vy_cm)/(gamma_lab*(1+n2.vy_cm*v_lab/pow(c_,2)));
	n2.vz_lab = (n2.vz_cm+v_lab)/(1+n2.vz_cm*v_lab/pow(c_,2));
	n2.z = darray + r.Exp(att_length);	
	n2.t = (n2.z)/(n2.vz_lab);
	n2.x = (n2.vx_lab)*(n2.t);
        n2.y = (n2.vy_lab)*(n2.t);
	n2.x_mona = n2.x + r.Gaus(0,3);
	n2.y_mona = round(n2.y/10)*10;
	n2.z_mona = int(n2.z/10)*10 + 5;
	
	n3.vx_lab = (n3.vx_cm)/(gamma_lab*(1+n3.vx_cm*v_lab/pow(c_,2)));
	n3.vy_lab = (n3.vy_cm)/(gamma_lab*(1+n3.vy_cm*v_lab/pow(c_,2)));
	n3.vz_lab = (n3.vz_cm+v_lab)/(1+n3.vz_cm*v_lab/pow(c_,2));
	n3.z = darray + r.Exp(att_length);	
	n3.t = (n3.z)/(n3.vz_lab);
	n3.x = (n3.vx_lab)*(n3.t);
        n3.y = (n3.vy_lab)*(n3.t);
	n3.x_mona = n3.x + r.Gaus(0,mona_res);
	n3.y_mona = round(n3.y/10)*10;
	n3.z_mona = int(n3.z/10)*10 + 5;

	// time order the three neutrons into hit vectors
	Int_t i=0, j=0, k=0;
/*	if (n1.t<n2.t) {
		if (n1.t<n3.t) { i=1; if(n2.t<n3.t) { j=2; k=3; } else { j=3; k=2; }
		} else { i=2; j=3; k=1; }
	} else {
		if (n2.t<n3.t) { j=1; if (n1.t<n3.t) { i=2; k=3; } else { i=3; k=2; }
		} else { i=3; j=2; k=1; }
	}*/

	if (n2.t<n3.t) {
		j=2; k=3; 
	} else j=3; k=2; 
// 	hit.t_hit[i-1]=n1.t; hit.x_hit[i-1]=n1.x_mona; hit.y_hit[i-1]=n1.y_mona; hit.z_hit[i-1]=n1.z_mona; 
//		hit.vx_hit[i-1]=n1.vx_lab; hit.vy_hit[i-1]=n1.vy_lab; hit.vz_hit[i-1]=n1.vz_lab; hit.edecay_hit[i-1]=n1.edecay; hit.vdecay_hit[i-1]=n1.vdecay;	
 	hit.t_hit[j-1]=n2.t; hit.x_hit[j-1]=n2.x_mona; hit.y_hit[j-1]=n2.y_mona; hit.z_hit[j-1]=n2.z_mona;
		hit.vx_hit[j-1]=n2.vx_lab; hit.vy_hit[j-1]=n2.vy_lab; hit.vz_hit[j-1]=n2.vz_lab; hit.edecay_hit[j-1]=n2.edecay; hit.vdecay_hit[j-1]=n2.vdecay;	
 	hit.t_hit[k-1]=n3.t; hit.x_hit[k-1]=n3.x_mona; hit.y_hit[k-1]=n3.y_mona; hit.z_hit[k-1]=n3.z_mona;
		hit.vx_hit[k-1]=n3.vx_lab; hit.vy_hit[k-1]=n3.vy_lab; hit.vz_hit[k-1]=n3.vz_lab; hit.edecay_hit[k-1]=n3.edecay; hit.vdecay_hit[k-1]=n3.vdecay;	

	if(i!=j && i!=k && j!=k) {
       		t->Fill();
	}
   }

ofile->Write();
ofile->Close();
config_file.close();
}

