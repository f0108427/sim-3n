// *****
// sim-3n.cpp 
// Simulation for 3 neutrons using ROOT random function generators parameterized by BreitWigner and Uniform
//
// W.F. Rogers 4/2022
//
// For future development:
// 	Single neutron scattering according to LANSCE results
// 	Edges to MoNA to allow neutrons to pass through
// 	Include fragment recoil - done
// 	Include target straggling and energy loss?
//
// Future uses to include:
// 	Assess effect of causality filters on decay energy results
// 	Create ANSI plots for 1 and 2 neutron signatures (cleaner than current STMONA simulations)
//
//
// Still need to add: 
// 	20230626 time resolution for MoNA
//
// *****

#include <fstream>
#include <string>
#include <iostream>
#include <sstream>
#include <math.h>
#include "sim-3n.h"

// root header files
#include </home/rogers/root/include/TFile.h>
#include </home/rogers/root/include/TTree.h>
#include </home/rogers/root/include/TTreeIndex.h>
#include </home/rogers/root/include/TBranchElement.h>
#include </home/rogers/root/include/TH1.h>
#include "/home/rogers/root/include/TRandom.h"
#include "/home/rogers/root/include/TCanvas.h"

using namespace std;

int main(int argc, char** argv)
{
  if(argc < 2){ 
	  cout << "Usage:   " << argv[0] << ",  outfile.root,  configfile.txt (optional)" << endl;
	  return 0; 
  }

  Double_t gamma_vcm, v_beam, v_cm;
  Double_t c_ = 29.979; // cm/ns
  Double_t amu = 1*931.49432;
  Double_t c2 = c_*c_;
  Double_t mn_c2 = 939.565; // MeV
  Double_t f1_amu = 14.0;
  Double_t f2_amu = 13.0;
  Double_t f3_amu = 12.0;
  Double_t mf1_c2 = f1_amu*amu;
  Double_t mf2_c2 = f2_amu*amu;
  Double_t mf3_c2 = f3_amu*amu;
  Double_t ebeam, darray, edecay_peak_1, edecay_width_1, edecay_peak_2, edecay_width_2, edecay_peak_3, edecay_width_3;
  Double_t first_peak, _3body_peak, _3body_width, ps_peak, ps_width;
  Double_t att_length, mona_res, decaybranch = 0;
  Int_t mult, nevents;
  string inputline;

  ifstream config_file;
  string config_file_name[2];
  if(argc == 3){ //check for optional config file
    config_file_name[1] = argv[2];
  } else {
    config_file_name[1] = "sim-3n.cfg"; //default config file name
    config_file_name[0] = "~/";
  }

  config_file.open(config_file_name[1],ios::in);
  string inputline1;
  string inputline2;
  string inputline3;
  string inputline4;
  string inputline5;

  if (config_file.is_open()) {
    getline(config_file,inputline1);
    istringstream in1(inputline1);
    in1 >> ebeam >> darray >> mona_res >> att_length >> nevents;

    getline(config_file,inputline2); 
    istringstream in2(inputline2);
    in2 >> edecay_peak_1 >> edecay_width_1;
    
    getline(config_file,inputline3); 
    istringstream in3(inputline3);
    in3 >> edecay_peak_2 >> edecay_width_2;
    
    getline(config_file,inputline4); 
    istringstream in4(inputline4);
    in4 >> edecay_peak_3 >> edecay_width_3;

    getline(config_file,inputline5); 
    istringstream in5(inputline5);
    in5 >> _3body_peak >> _3body_width >> ps_peak >> ps_width >> decaybranch;

  } else {
    cout << "Config file did not open properly" << endl;
    return 0;
  }

  v_beam = c_*sqrt(1-(pow(mn_c2/(ebeam+mn_c2),2)));
  v_cm = v_beam*(mf1_c2/(mf1_c2+mn_c2));
  gamma_vcm = sqrt(1/(1-pow(v_cm/c_,2)));

  cout << " ebeam/A: " << ebeam << " v_cm: " << v_cm << " gamma_vcm: " << gamma_vcm << " darray: " << darray << " mona_res: " << mona_res << " att_length: " << att_length << " nevents: " << nevents << endl;
  cout << " edecay_peak_1: " << edecay_peak_1 << " edecay_width_1: " << edecay_width_1 << endl;
  cout << " edecay_peak_2: " << edecay_peak_2 << " edecay_width_2: " << edecay_width_2 << endl;
  cout << " edecay_peak_3: " << edecay_peak_3 << " edecay_width_3: " << edecay_width_3 << endl;
  cout << " 3body_peak: " << _3body_peak << " 3body_width: " << _3body_width << " 1st ps neutron peak: " << ps_peak << " 1st ps neutron width: " << ps_width << " decaybranch: " << decaybranch << endl;

  TRandom r;
  sim_3n::Create_neutron n1; 
  sim_3n::Create_neutron n2; 
  sim_3n::Create_neutron n3;
  sim_3n::Create_fragment f1; 
  sim_3n::Create_fragment f2; 
  sim_3n::Create_fragment f3; 
  sim_3n::Create_hit hit;
  
  char* outfile = argv[1];  
  TFile* ofile = new TFile(outfile,"recreate");

  TTree *t = new TTree("t","3n-sim tree");
  t->Branch("n1",&n1,"t/D:x/D:y/D:z/D:x_mona/D:y_mona/D:z_mona/D:vx_cm/D:vy_cm/D:vz_cm/D:v_cm/D:vx_lab/D:vy_lab/D:vz_lab/D:v_lab/D:tx/D:ty/D:edecay/D:vdecay/D");
  t->Branch("n2",&n2,"t/D:x/D:y/D:z/D:x_mona/D:y_mona/D:z_mona/D:vx_cm/D:vy_cm/D:vz_cm/D:v_cm/D:vx_lab/D:vy_lab/D:vz_lab/D:v_lab/D:tx/D:ty/D:edecay/D:vdecay/D");
  t->Branch("n3",&n3,"t/D:x/D:y/D:z/D:x_mona/D:y_mona/D:z_mona/D:vx_cm/D:vy_cm/D:vz_cm/D:v_cm/D:vx_lab/D:vy_lab/D:vz_lab/D:v_lab/D:tx/D:ty/D:edecay/D:vdecay/D");
  t->Branch("f1",&f1,"vx_cm/D:vy_cm/D:vz_cm/D:v_cm/D:vx_lab/D:vy_lab/D:vz_lab/D:v_lab/D:tx/D:ty/D");
  t->Branch("f2",&f2,"vx_cm/D:vy_cm/D:vz_cm/D:v_cm/D:vx_lab/D:vy_lab/D:vz_lab/D:v_lab/D:tx/D:ty/D");
  t->Branch("f3",&f3,"vx_cm/D:vy_cm/D:vz_cm/D:v_cm/D:vx_lab/D:vy_lab/D:vz_lab/D:v_lab/D:tx/D:ty/D");
  t->Branch("hit",&hit,"t[3]/D:x[3]/D:y[3]/D:z[3]/D:vx[3]/D:vy[3]/D:vz[3]/D:v/D");
  t->Branch("mult",&mult,"mult/I");


  for (Int_t num_evt=0;num_evt<nevents;num_evt++) {
	mult = 0;
// Determine neutron kinetic energies of decay
	n1.edecay = r.BreitWigner(edecay_peak_1, edecay_width_1); // edecay value based on random sampling over Breit-Wigner lineishape 
//	n1.vdecay = mf1_c2/(mf1_c2+mn_c2)*c_*sqrt(1-pow(mn_c2/(n1.edecay+mn_c2),2)); // velocity of decaying neutron with Be14 fragment
	n1.vdecay = c_*sqrt((mf1_c2/(mf1_c2+mn_c2))*2*n1.edecay/mn_c2); // velocity of decaying neutron with Be14 fragment
       	r.Sphere(n1.vx_cm, n1.vy_cm, n1.vz_cm, n1.vdecay); // randomize directions of velocity vectors with tips over surface of sphere
        n1.v_cm = sqrt(pow(n1.vx_cm,2)+pow(n1.vy_cm,2)+pow(n1.vz_cm,2));
	f1.vx_cm = -(mn_c2/mf1_c2)*n1.vx_cm; // Be14 fragment
	f1.vy_cm = -(mn_c2/mf1_c2)*n1.vy_cm;
	f1.vz_cm = -(mn_c2/mf1_c2)*n1.vz_cm;
        f1.v_cm = sqrt(pow(f1.vx_cm,2)+pow(f1.vy_cm,2)+pow(f1.vz_cm,2));

// Second neutron (first of two phase space neutrons)
	n2.edecay = r.BreitWigner(edecay_peak_2, edecay_width_2); // 
	n2.vdecay = c_*sqrt((mf2_c2/(mf2_c2+mn_c2))*2*n2.edecay/mn_c2); // velocity of decaying neutron with Be13 fragment
       	r.Sphere(n2.vx_cm, n2.vy_cm, n2.vz_cm, n2.vdecay);

	f2.vx_cm = -(mn_c2/mf2_c2)*n2.vx_cm + f1.vx_cm; // Be13 fragment goes in opposite direction of the neutron with a fraction of the velocity
	f2.vy_cm = -(mn_c2/mf2_c2)*n2.vy_cm + f1.vy_cm;
	f2.vz_cm = -(mn_c2/mf2_c2)*n2.vz_cm + f1.vz_cm;
        f2.v_cm = sqrt(pow(f2.vx_cm,2)+pow(f2.vy_cm,2)+pow(f2.vz_cm,2));
	n2.vx_cm += f1.vx_cm; // Add fragment velocity from first decay
	n2.vy_cm += f1.vy_cm;
	n2.vz_cm += f1.vz_cm;
        n2.v_cm = sqrt(pow(n2.vx_cm,2)+pow(n2.vy_cm,2)+pow(n2.vz_cm,2));

// Second neutron (second of two phase space neutrons)
//	n3.edecay = r.BreitWigner(_3body_peak, _3body_width) - n2.edecay; // 2n phase space decay
	n3.edecay = r.BreitWigner(edecay_peak_3, edecay_width_3); // sequential decay
	n3.vdecay = c_*sqrt((mf3_c2/(mf3_c2+mn_c2))*2*n3.edecay/mn_c2); // velocity of decaying neutron with Be13 fragment
       	r.Sphere(n3.vx_cm, n3.vy_cm, n3.vz_cm, n3.vdecay);

	f3.vx_cm = -(mn_c2/mf3_c2)*n3.vx_cm + f2.vx_cm; // Be13 fragment goes in opposite direction of the neutron with a fraction of the velocity
	f3.vy_cm = -(mn_c2/mf3_c2)*n3.vy_cm + f2.vy_cm;
	f3.vz_cm = -(mn_c2/mf3_c2)*n3.vz_cm + f2.vz_cm;
        f3.v_cm = sqrt(pow(f3.vx_cm,2)+pow(f3.vy_cm,2)+pow(f3.vz_cm,2));
	n3.vx_cm += f2.vx_cm; // Add fragment velocity from first decay
	n3.vy_cm += f2.vy_cm;
	n3.vz_cm += f2.vz_cm;
        n3.v_cm = sqrt(pow(n3.vx_cm,2)+pow(n3.vy_cm,2)+pow(n3.vz_cm,2));

// Transform quantities to the laboratory frame
// Neutron 1
	n1.vx_lab = (n1.vx_cm)/(gamma_vcm*(1+n1.vz_cm*v_cm/c2));
	n1.vy_lab = (n1.vy_cm)/(gamma_vcm*(1+n1.vz_cm*v_cm/c2));
	n1.vz_lab = (n1.vz_cm+v_cm)/(1+n1.vz_cm*v_cm/c2);
        n1.v_lab = sqrt(pow(n1.vx_lab,2)+pow(n1.vy_lab,2)+pow(n1.vz_lab,2));
	n1.tx = atan(n1.vx_lab/n1.vz_lab);
	n1.ty = atan(n1.vy_lab/n1.vz_lab);
	
	f1.vx_lab = (f1.vx_cm)/(gamma_vcm*(1+f1.vz_cm*v_cm/c2));
	f1.vy_lab = (f1.vy_cm)/(gamma_vcm*(1+f1.vz_cm*v_cm/c2));
	f1.vz_lab = (f1.vz_cm+v_cm)/(1+f1.vz_cm*v_cm/c2);
        f1.v_lab = sqrt(pow(f1.vx_lab,2)+pow(f1.vy_lab,2)+pow(f1.vz_lab,2));
	f1.tx = atan(f1.vx_lab/f1.vz_lab);
	f1.ty = atan(f1.vy_lab/f1.vz_lab);
	
	n1.z = darray + r.Exp(att_length);	
	n1.t = (n1.z)/(n1.vz_lab);
	n1.x = (n1.vx_lab)*(n1.t);
        n1.y = (n1.vy_lab)*(n1.t);
	n1.x_mona = n1.x + r.Gaus(0,3); // add detector resolution
	n1.y_mona = round(n1.y/10.0)*10.0;
	n1.z_mona = int(n1.z/10.0)*10.0 + 5;

// Neutron 2
	n2.vx_lab = (n2.vx_cm)/(gamma_vcm*(1+n2.vz_cm*v_cm/c2));
	n2.vy_lab = (n2.vy_cm)/(gamma_vcm*(1+n2.vz_cm*v_cm/c2));
	n2.vz_lab = (n2.vz_cm+v_cm)/(1+n2.vz_cm*v_cm/c2);
        n2.v_lab = sqrt(pow(n2.vx_lab,2)+pow(n2.vy_lab,2)+pow(n2.vz_lab,2));
	n2.tx = atan(n2.vx_lab/n2.vz_lab);
	n2.ty = atan(n2.vy_lab/n2.vz_lab);
	
	f2.vx_lab = (f2.vx_cm)/(gamma_vcm*(1+f2.vz_cm*v_cm/c2));
	f2.vy_lab = (f2.vy_cm)/(gamma_vcm*(1+f2.vz_cm*v_cm/c2));
	f2.vz_lab = (f2.vz_cm+v_cm)/(1+f2.vz_cm*v_cm/c2);
        f2.v_lab = sqrt(pow(f2.vx_cm,2)+pow(f2.vy_cm,2)+pow(f2.vz_lab,2));
	f2.tx = atan(f2.vx_lab/f2.vz_lab);
	f2.ty = atan(f2.vy_lab/f2.vz_lab);
	
	n2.z = darray + r.Exp(att_length);	
	n2.t = (n2.z)/(n2.vz_lab);
	n2.x = (n2.vx_lab)*(n2.t);
        n2.y = (n2.vy_lab)*(n2.t);
	n2.x_mona = n2.x + r.Gaus(0,3);
	n2.y_mona = round(n2.y/10.0)*10.0;
	n2.z_mona = int(n2.z/10.0)*10.0 + 5.0;
	
// Neutron 3
	n3.vx_lab = (n3.vx_cm)/(gamma_vcm*(1+n3.vz_cm*v_cm/c2));
	n3.vy_lab = (n3.vy_cm)/(gamma_vcm*(1+n3.vz_cm*v_cm/c2));
	n3.vz_lab = (n3.vz_cm+v_cm)/(1+n3.vz_cm*v_cm/c2);
        n3.v_lab = sqrt(pow(n3.vx_lab,2)+pow(n3.vy_lab,2)+pow(n3.vz_lab,2));
	n3.tx = atan(n3.vx_lab/n3.vz_lab);
	n3.ty = atan(n3.vy_lab/n3.vz_lab);

	f3.vx_lab = (f3.vx_cm)/(gamma_vcm*(1+f3.vz_cm*v_cm/c2));
	f3.vy_lab = (f3.vy_cm)/(gamma_vcm*(1+f3.vz_cm*v_cm/c2));
	f3.vz_lab = (f3.vz_cm+v_cm)/(1+f3.vz_cm*v_cm/c2);
        f3.v_lab = sqrt(pow(f3.vx_lab,2)+pow(f3.vy_lab,2)+pow(f3.vz_lab,2));
	f3.tx = atan(f3.vx_lab/f3.vz_lab);
	f3.ty = atan(f3.vy_lab/f3.vz_lab);

	n3.z = darray + r.Exp(att_length);	
	n3.t = (n3.z)/(n3.vz_lab);
	n3.x = (n3.vx_lab)*(n3.t);
        n3.y = (n3.vy_lab)*(n3.t);
	n3.x_mona = n3.x + r.Gaus(0,mona_res);
	n3.y_mona = round(n3.y/10.0)*10.0;
	n3.z_mona = int(n3.z/10.0)*10.0 + 5.0;

// time order the three neutrons into hit vectors
	if (r.Uniform(1) < decaybranch) { // decaybranch = 1.0 -- all 3n, 0.0 -- all 2n
	  mult = 3;
	  Int_t i=0, j=0, k=0;
	  if (n1.t<n2.t) {
	  	if (n1.t<n3.t) { i=1; if(n2.t<n3.t) { j=2; k=3; } else { j=3; k=2; }
		} else { i=2; j=3; k=1; }
	  } else {
		if (n2.t<n3.t) { j=1; if (n1.t<n3.t) { i=2; k=3; } else { i=3; k=2; }
		} else { i=3; j=2; k=1; }
	  }
	  //cout << "hit order = " << i << " " << j << " " << k << endl;
 	  hit.t[i-1]=n1.t; hit.x[i-1]=n1.x_mona; hit.y[i-1]=n1.y_mona; hit.z[i-1]=n1.z_mona; 
		hit.vx[i-1]=n1.vx_lab; hit.vy[i-1]=n1.vy_lab; hit.vz[i-1]=n1.vz_lab;
        	hit.v[i-1] = sqrt(pow(hit.vx[i-1],2)+pow(hit.vy[i-1],2)+pow(hit.vz[i-1],2));
 	  hit.t[j-1]=n2.t; hit.x[j-1]=n2.x_mona; hit.y[j-1]=n2.y_mona; hit.z[j-1]=n2.z_mona;
		hit.vx[j-1]=n2.vx_lab; hit.vy[j-1]=n2.vy_lab; hit.vz[j-1]=n2.vz_lab;
        	hit.v[j-1] = sqrt(pow(hit.vx[j-1],2)+pow(hit.vy[j-1],2)+pow(hit.vz[j-1],2));
 	  hit.t[k-1]=n3.t; hit.x[k-1]=n3.x_mona; hit.y[k-1]=n3.y_mona; hit.z[k-1]=n3.z_mona;
		hit.vx[k-1]=n3.vx_lab; hit.vy[k-1]=n3.vy_lab; hit.vz[k-1]=n3.vz_lab;
        	hit.v[k-1] = sqrt(pow(hit.vx[k-1],2)+pow(hit.vy[k-1],2)+pow(hit.vz[k-1],2));
	} else {
	  mult = 2;
	  Int_t j=0, k=0;
	  if (n2.t<n3.t) {j=1; k=2;} else {j=2; k=1;}
	  hit.t[j-1]=n2.t; hit.x[j-1]=n2.x_mona; hit.y[j-1]=n2.y_mona; hit.z[j-1]=n2.z_mona;
		hit.vx[j-1]=n2.vx_lab; hit.vy[j-1]=n2.vy_lab; hit.vz[j-1]=n2.vz_lab; 
 	  hit.t[k-1]=n3.t; hit.x[k-1]=n3.x_mona; hit.y[k-1]=n3.y_mona; hit.z[k-1]=n3.z_mona;
		hit.vx[k-1]=n3.vx_lab; hit.vy[k-1]=n3.vy_lab; hit.vz[k-1]=n3.vz_lab;
	}
       	t->Fill();

  }

  t->Write();
  ofile->Write();
  ofile->Close();
  config_file.close();
}

